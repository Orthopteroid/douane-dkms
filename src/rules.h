#ifndef _RULES_H_
#define _RULES_H_

/*
**  Module internal rules_list management
*
*   This is all the Douane's rules_list management code to add/remove/search rules_list.
*/
// TODO : Protect all those functions with RCU
struct rule
{
  char              process_path[PATH_LENGTH];
  bool              allowed;
  struct list_head  list;
  struct rcu_head   rcu;
};

#ifdef DEBUG
void rules_print_rcu(void);
#endif

void rules_append_rcu(const char * process_path, const bool is_allowed);
void rules_clear_rcu(void);
void rules_remove_rcu(const unsigned char * process_path);
struct rule * rules_search_rcu(const unsigned char * process_path, const uint32_t packet_id);

#endif // _RULES_H_
