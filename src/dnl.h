#ifndef _DNL_H_
#define _DNL_H_

typedef void(*dnl_inputfn)(struct sk_buff *skb);

int dnl_initialize_socket(dnl_inputfn ifn);
void dnl_shutdown_socket(void);
int dnl_notify_douane_daemon(const struct network_activity * activity, const uint32_t packet_id);

extern struct sock *  dnl_activities_socket;
extern pid_t          dnl_daemon_pid;

#endif // _DNL_H_
