// Douane kernel module
// Copyright (C) 2013  Guillaume Hain <zedtux@zedroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <linux/module.h>         // Needed by all modules
#include <linux/kernel.h>         // Needed for KERN_INFO
#include <linux/version.h>        // Needed for LINUX_VERSION_CODE >= KERNEL_VERSION
// ~~~~ Due to bug https://bugs.launchpad.net/ubuntu/+source/linux/+bug/929715 ~~~~
// #undef __KERNEL__
// #include <linux/netfilter_ipv4.h> // NF_IP_POST_ROUTING, NF_IP_PRI_LAST
// #define __KERNEL__
#define NF_IP_LOCAL_OUT 3
enum nf_ip_hook_priorities {
  NF_IP_PRI_LAST = INT_MAX
};
// ~~~~
#include <linux/netdevice.h>	    // net_device
#include <linux/netfilter.h>      // nf_register_hook(), nf_unregister_hook(), nf_register_net_hook(), nf_unregister_net_hook()
#include <linux/netlink.h>        // NLMSG_SPACE(), nlmsg_put(), NETLINK_CB(), NLMSG_DATA(), NLM_F_REQUEST, netlink_unicast(), netlink_kernel_release(), nlmsg_hdr(), NETLINK_USERSOCK, netlink_kernel_create()

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
#include <linux/signal.h>
#else
#include <linux/sched/signal.h>          // for_each_process(), task_lock(), task_unlock()
#endif

#include <linux/ip.h>             // ip_hdr()
#include <linux/udp.h>            // udp_hdr()
#include <linux/tcp.h>            // tcp_hdr()
#include <linux/fdtable.h>        // files_fdtable(), fcheck_files()
#include <linux/list.h>           // INIT_LIST_HEAD(), list_for_each_entry(), list_add_tail(), list_empty(), list_entry(), list_del(), list_for_each_entry_safe()
#include <linux/dcache.h>         // d_path()
#include <linux/skbuff.h>         // alloc_skb()
#include <linux/pid_namespace.h>  // task_active_pid_ns()
#include <linux/rculist.h>        // hlist_for_each_entry_rcu

#include "network_activity_message.h"

#include "rules.h"

DEFINE_SPINLOCK(rules_lock);
LIST_HEAD(rules_list);

#ifdef DEBUG
int rules_count = 0;

void rules_print_rcu(void)
{
  struct rule * current_rule;

  pr_debug("douane:%d:%s: Currently %d rules_list are regisered.\n", __LINE__, __FUNCTION__, (int) rules_count);

  // Iterate over all registered rules_list
  rcu_read_lock();
  list_for_each_entry_rcu(current_rule, &rules_list, list)
  {
    if (current_rule->allowed)
    {
      pr_debug("douane:%d:%s: process_path %s is allowed.\n", __LINE__, __FUNCTION__, current_rule->process_path);
    } else {
      pr_debug("douane:%d:%s: process_path %s is disallowed.\n", __LINE__, __FUNCTION__, current_rule->process_path);
    }
  }
  rcu_read_unlock();
}
#endif

void rules_append_rcu(const char * process_path, const bool is_allowed)
{
  struct rule * new_rule;

  // Don't do anything if passed process_path is NULL
  if (process_path == NULL)
    return;

  // Don't do anything if the process_path length is > PATH_LENGTH
  if (strlen(process_path) > PATH_LENGTH)
    return;

  new_rule = (struct rule *)kmalloc(sizeof(struct rule), GFP_ATOMIC);
  if(new_rule == NULL)
  {
    pr_err("douane:%d:%s: Failed to allocate new rule.\n", __LINE__, __FUNCTION__);
    return;
  }

  // Copy the passed path in the rule
  strncpy(new_rule->process_path, process_path, PATH_LENGTH);
  new_rule->allowed = is_allowed;

  // Add the rule to the linked list
  spin_lock(&rules_lock);
  list_add_tail_rcu(&new_rule->list, &rules_list);
  spin_unlock(&rules_lock);
#ifdef DEBUG
  // Update rules_list counter
  rules_count++;
#endif

#ifdef DEBUG
  if (new_rule->allowed)
  {
    pr_info("douane:%d:%s: New rule of type ALLOW added for %s\n", __LINE__, __FUNCTION__, new_rule->process_path);
  } else {
    pr_info("douane:%d:%s: New rule of type DISALLOW added for %s\n", __LINE__, __FUNCTION__, new_rule->process_path);
  }
  rules_print_rcu();
#endif
}

void rules_clear_rcu(void)
{
  struct rule * rule;
  int           rule_cleaned_records = 0;

  if (list_empty(&rules_list))
    return;

  pr_debug("douane:%d:%s: Clearing rules_list ...\n", __LINE__, __FUNCTION__);

  spin_lock(&rules_lock);
  list_for_each_entry(rule, &rules_list, list)
  {
    list_del_rcu(&rule->list);
    spin_unlock(&rules_lock);

    synchronize_rcu();
    kfree(rule);

    rule_cleaned_records++;
  }
  spin_unlock(&rules_lock);

  pr_debug("douane:%d:%s: %d rule(s) successfully cleaned.\n", __LINE__, __FUNCTION__, rule_cleaned_records);

#ifdef DEBUG
  // Reset rules_list counter
  rules_count = 0;
#endif
}

void rules_remove_rcu(const unsigned char * process_path)
{
  struct rule * rule;

  // Don't do anything if passed process_path is NULL
  if (process_path == NULL)
    return;

  // Don't do anything if the process_path length is > PATH_LENGTH
  if (strlen(process_path) > PATH_LENGTH)
    return;

  pr_debug("douane:%d:%s: Deleting rule for path %s.\n", __LINE__, __FUNCTION__, process_path);

  // Iterate over all registered rules_list
  spin_lock(&rules_lock);
  list_for_each_entry(rule, &rules_list, list)
  {
    // Compare current rule process_path with the passed one
    if (strncmp(rule->process_path, process_path, PATH_LENGTH) == 0)
    {
      list_del_rcu(&rule->list);
      spin_unlock(&rules_lock);

      /*
      ** This module can't block, therefore it can't use synchronize_rcu() and
      *  kfree(), instead it uses kfree_rcu() which creates a callback function
      *  to free the given list entry while in a grace period.
      */
      kfree_rcu(rule, rcu);

      pr_debug("douane:%d:%s: Rule for process_path %s successfully deleted\n", __LINE__, __FUNCTION__, process_path);
      return;

#ifdef DEBUG
      // Update rules_list counter
      rules_count--;
#endif
    }
  }
  spin_unlock(&rules_lock);

  pr_warn("douane:%d:%s: No rule for process_path %s deleted\n", __LINE__, __FUNCTION__, process_path);
}

struct rule * rules_search_rcu(const unsigned char * process_path, const uint32_t packet_id)
{
  struct rule * current_rule;

  // Don't do anything if passed process_path is NULL
  if (process_path == NULL)
    return NULL;

  // Don't do anything if the process_path length is > PATH_LENGTH
  if (strlen(process_path) > PATH_LENGTH)
    return NULL;

  // Iterate over all registered rules_list
  rcu_read_lock();
  list_for_each_entry_rcu(current_rule, &rules_list, list)
  {
    // Compare current rule process_path with the passed one
    if (strncmp(current_rule->process_path, process_path, PATH_LENGTH) == 0)
    {
#ifdef DEBUG
      if (current_rule->allowed)
      {
        pr_debug("douane(%u):%d:%s: Rule ALLOW found for %s.\n", packet_id, __LINE__, __FUNCTION__, current_rule->process_path);
      } else {
        pr_debug("douane(%u):%d:%s: Rule DISALLOW found for %s.\n", packet_id, __LINE__, __FUNCTION__, current_rule->process_path);
      }
#endif
      rcu_read_unlock();

      // Return the rule when found (Stop the loop)
      return current_rule;
    }
  }
  rcu_read_unlock();

  pr_debug("douane(%u):%d:%s: rule not found for process_path %s ...\n", packet_id, __LINE__, __FUNCTION__, process_path);

  // Otherwise return a NULL (Rule not found)
  return NULL;
}
