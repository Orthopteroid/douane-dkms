// Douane kernel module
// Copyright (C) 2013  Guillaume Hain <zedtux@zedroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <linux/module.h>         // Needed by all modules
#include <linux/kernel.h>         // Needed for KERN_INFO
#include <linux/version.h>        // Needed for LINUX_VERSION_CODE >= KERNEL_VERSION
// ~~~~ Due to bug https://bugs.launchpad.net/ubuntu/+source/linux/+bug/929715 ~~~~
// #undef __KERNEL__
// #include <linux/netfilter_ipv4.h> // NF_IP_POST_ROUTING, NF_IP_PRI_LAST
// #define __KERNEL__
#define NF_IP_LOCAL_OUT 3
enum nf_ip_hook_priorities {
  NF_IP_PRI_LAST = INT_MAX
};
// ~~~~
#include <linux/netdevice.h>	    // net_device
#include <linux/netfilter.h>      // nf_register_hook(), nf_unregister_hook(), nf_register_net_hook(), nf_unregister_net_hook()
#include <linux/netlink.h>        // NLMSG_SPACE(), nlmsg_put(), NETLINK_CB(), NLMSG_DATA(), NLM_F_REQUEST, netlink_unicast(), netlink_kernel_release(), nlmsg_hdr(), NETLINK_USERSOCK, netlink_kernel_create()

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
#include <linux/signal.h>
#else
#include <linux/sched/signal.h>          // for_each_process(), task_lock(), task_unlock()
#endif

#include <linux/ip.h>             // ip_hdr()
#include <linux/udp.h>            // udp_hdr()
#include <linux/tcp.h>            // tcp_hdr()
#include <linux/fdtable.h>        // files_fdtable(), fcheck_files()
#include <linux/list.h>           // INIT_LIST_HEAD(), list_for_each_entry(), list_add_tail(), list_empty(), list_entry(), list_del(), list_for_each_entry_safe()
#include <linux/dcache.h>         // d_path()
#include <linux/skbuff.h>         // alloc_skb()
#include <linux/pid_namespace.h>  // task_active_pid_ns()
#include <linux/rculist.h>        // hlist_for_each_entry_rcu

#include "network_activity_message.h"

#include "dnl.h"

struct sock *  dnl_activities_socket = NULL; // Netfilter socket to send network activities and recieve orders from the user space daemon

DEFINE_SPINLOCK(netlink_lock); // guard for the netlink socket reassignment

pid_t dnl_daemon_pid;               // User space running daemon

// Push in the Netlink socket the network activity to send it to the daemon
int dnl_notify_douane_daemon(const struct network_activity * activity, const uint32_t packet_id)
{
  struct nlmsghdr * nlh;
  struct sk_buff *  skb = NULL;
  int               ret = 0;

  if (dnl_activities_socket == NULL)
  {
    pr_err("douane(%u):%d:%s: BLOCKED PUSH: Socket not connected!!.\n", packet_id, __LINE__, __FUNCTION__);
    return -1;
  }

  // If no process_path don't send the network_activity message to the daemon
  if (activity->process_path == NULL || strcmp(activity->process_path, "") == 0)
  {
    pr_err("douane(%u):%d:%s: BLOCKED PUSH: process_path is blank.\n", packet_id, __LINE__, __FUNCTION__);
    return 0;
  }

  // If process_path is too long, don't send the network_activity message to the daemon
  if (strlen(activity->process_path) > PATH_LENGTH)
  {
    pr_err("douane(%u):%d:%s: BLOCKED PUSH: process_path is too long.\n", packet_id, __LINE__, __FUNCTION__);
  }

  // Create a new netlink message
  skb = nlmsg_new(NLMSG_ALIGN(sizeof(struct network_activity)) + nla_total_size(1), GFP_KERNEL);
  if (skb == NULL)
  {
    pr_err("douane(%u):%d:%s: BLOCKED PUSH: Failed to allocate new socket buffer.\n", packet_id, __LINE__, __FUNCTION__);
    return -1;
  }

  // Add a netlink message to an skb
  nlh = nlmsg_put(skb, 0, 0, NLMSG_DONE, sizeof(struct network_activity), 0);
  if (nlh == NULL)
  {
    if (skb)
      kfree_skb(skb);
    pr_err("douane(%u):%d:%s: BLOCKED PUSH: nlmsg_put failed.\n", packet_id, __LINE__, __FUNCTION__);
    return -1;
  }

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,7,0)
  NETLINK_CB(skb).portid = 0; /* from kernel */
#else
  NETLINK_CB(skb).pid = 0; /* from kernel */
#endif
  memcpy(NLMSG_DATA(nlh), activity, sizeof(struct network_activity));

  nlh->nlmsg_flags = NLM_F_REQUEST; // Must be set on all request messages.

  // Unicast a message to a single socket
  // netlink_unicast() takes ownership of the skb and frees it itself.
  spin_lock(&netlink_lock);
  if(dnl_activities_socket)
  {
    ret = netlink_unicast(dnl_activities_socket, skb, dnl_daemon_pid, MSG_DONTWAIT);
  }
  spin_unlock(&netlink_lock);

  if (ret < 0)
  {
    if (ret == -11)
    {
      pr_warn("douane(%u):%d:%s: Message ignored as Netfiler socket is busy.\n", packet_id, __LINE__, __FUNCTION__);
    } else {
      pr_err("douane(%u):%d:%s: Failed to send message (errno: %d).\n", packet_id, __LINE__, __FUNCTION__, ret);
      // No more try to push rules to the daemon
      dnl_daemon_pid = 0;
    }
    return ret;
  } else {
    return 0;
  }
}

//  Netlink socket initializer
int dnl_initialize_socket(dnl_inputfn ifn)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,6,0)
  struct netlink_kernel_cfg cfg =
  {
    .input = ifn,
  };
#endif

  spin_lock(&netlink_lock);

  if(dnl_activities_socket)
  {
    netlink_kernel_release(dnl_activities_socket);
  }

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,7,0)
  dnl_activities_socket = netlink_kernel_create(&init_net,
                        NETLINK_USERSOCK,
                        &cfg);
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(3,6,0)
  dnl_activities_socket = netlink_kernel_create(&init_net,
                        NETLINK_USERSOCK,
                        THIS_MODULE,
                        &cfg);
#else
  dnl_activities_socket = netlink_kernel_create(&init_net,
                        NETLINK_USERSOCK,
                        0,
                        ifn,
                        NULL,
                        THIS_MODULE);
#endif

  dnl_daemon_pid = 0;

  spin_unlock(&netlink_lock);

  if (dnl_activities_socket == NULL)
  {
    pr_err("douane:%d:%s: Can't create Netlink socket for network activities!\n", __LINE__, __FUNCTION__);
    return -ENOMEM;
  }

  return 0;
}

void dnl_shutdown_socket(void)
{
  pr_debug("douane:%d:%s: Shutting down activities Netlink socket...\n", __LINE__, __FUNCTION__);

  spin_lock(&netlink_lock);

  if (dnl_activities_socket)
  {
    netlink_kernel_release(dnl_activities_socket);
    dnl_activities_socket = NULL;
  }

  dnl_daemon_pid = 0;

  spin_unlock(&netlink_lock);

}
