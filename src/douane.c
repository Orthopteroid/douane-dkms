// Douane kernel module
// Copyright (C) 2013  Guillaume Hain <zedtux@zedroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <linux/module.h>         // Needed by all modules
#include <linux/kernel.h>         // Needed for KERN_INFO
#include <linux/version.h>        // Needed for LINUX_VERSION_CODE >= KERNEL_VERSION
// ~~~~ Due to bug https://bugs.launchpad.net/ubuntu/+source/linux/+bug/929715 ~~~~
// #undef __KERNEL__
// #include <linux/netfilter_ipv4.h> // NF_IP_POST_ROUTING, NF_IP_PRI_LAST
// #define __KERNEL__
#define NF_IP_LOCAL_OUT 3
enum nf_ip_hook_priorities {
  NF_IP_PRI_LAST = INT_MAX
};
// ~~~~
#include <linux/netdevice.h>	    // net_device
#include <linux/netfilter.h>      // nf_register_hook(), nf_unregister_hook(), nf_register_net_hook(), nf_unregister_net_hook()
#include <linux/netlink.h>        // NLMSG_SPACE(), nlmsg_put(), NETLINK_CB(), NLMSG_DATA(), NLM_F_REQUEST, netlink_unicast(), netlink_kernel_release(), nlmsg_hdr(), NETLINK_USERSOCK, netlink_kernel_create()

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
#include <linux/signal.h>
#else
#include <linux/sched/signal.h>          // for_each_process(), task_lock(), task_unlock()
#endif

#include <linux/ip.h>             // ip_hdr()
#include <linux/udp.h>            // udp_hdr()
#include <linux/tcp.h>            // tcp_hdr()
#include <linux/fdtable.h>        // files_fdtable(), fcheck_files()
#include <linux/list.h>           // INIT_LIST_HEAD(), list_for_each_entry(), list_add_tail(), list_empty(), list_entry(), list_del(), list_for_each_entry_safe()
#include <linux/dcache.h>         // d_path()
#include <linux/skbuff.h>         // alloc_skb()
#include <linux/pid_namespace.h>  // task_active_pid_ns()
#include <linux/rculist.h>        // hlist_for_each_entry_rcu

#include "network_activity_message.h"
#include "dnl.h"
#include "psi.h"
#include "rules.h"

#ifndef DOUANE_VERSION
#define DOUANE_VERSION "UNKNOWN"
#endif

MODULE_DESCRIPTION("Douane");
MODULE_AUTHOR("Guillaume Hain <zedtux@zedroot.org>");
MODULE_VERSION(DOUANE_VERSION);
MODULE_LICENSE("GPL v2");

// Limit of fd not found before to switch to the next process
#define MAX_FD_NULL 9

// This stopping boolean is used in order to prevent any futur processing of
// incoming packets when the module is about to be unloaded or in troubles.
bool stopping = false;

/*
** Helpers
*/
static int index_of(const char * base, const char * str)
{
  int result;
  int baselen = strlen(base);
  char * pos = NULL;

  // str should not longer than base
  if (strlen(str) > baselen)
    result = -1;
  else {
    pos = strstr(base, str);
    result = ((pos == NULL) ? -1 : (pos - base));
  }
  return result;
}


/*
**  Global module memory helper methods
*/
// blackout is called when:
//   - it is not possible to send messages to the daemon
//   - when stopping the daemon
//   - when unmonting this module
static void blackout(void)
{
  // Skip any futur packets
  stopping = true;

  psi_clear_rcu();
  rules_clear_rcu();
}

/*
** Netlink: Communications with user namespace
*/

// Receiving messages from the Douane daemon
static void activities_socket_receiver(struct sk_buff *skb)
{
  struct nlmsghdr *         nlh = nlmsg_hdr(skb);
  struct network_activity * activity = NLMSG_DATA(nlh);
  struct rule *             new_rule = NULL;

  if (activity == NULL)
  {
    pr_err("douane:%d:%s: Can't allocate memory for a new activity\n", __LINE__, __FUNCTION__);
    return;
  }

  // Activity kind must be positive number
  if (activity->kind <= 0)
  {
    pr_err("douane:%d:%s: Invalid received message. (No kind)\n", __LINE__, __FUNCTION__);
    dnl_initialize_socket(activities_socket_receiver);
    blackout();
    return;
  }

  // Do actions based on the kind
  switch(activity->kind)
  {
    // Opening connection to the LKM
    case KIND_HAND_CHECK:
    {
      dnl_daemon_pid = nlh->nlmsg_pid;
      pr_debug("douane:%d:%s: Process %d successfully registered.\n", __LINE__, __FUNCTION__, dnl_daemon_pid);
    }
    break;

    // Adding new rule to the LKM (used after connection hand check to load rules_list)
    case KIND_SENDING_RULE:
    {
      // Lookup for an exiting rule
      new_rule = rules_search_rcu(activity->process_path, 0);
      // No rule found for the process path
      if (new_rule == NULL)
      {
        // Create the new rule
        rules_append_rcu(activity->process_path, (activity->allowed == 1));
      }
    }
    break;

    // Delete a rule
    case KIND_DELETE_RULE:
    {
      rules_remove_rcu(activity->process_path);
      pr_debug("douane:%d:%s: Rule deleted\n", __LINE__, __FUNCTION__);
    }
    break;

    // Disconnecting from the LKM (properly)
    case KIND_GOODBYE:
    {
      dnl_daemon_pid = 0;
    }
    break;

    // All other kinds
    default:
    {
      pr_err("douane:%d:%s: Invalid received message. (Unknown kind)\n", __LINE__, __FUNCTION__);
      // Disconnect the userspace process that send a wrong message
      dnl_initialize_socket(activities_socket_receiver);
      blackout();
    }
    break;
  }
}


/*
**  Relation packet <-> process helper methods
*/
static bool task_owns_socket_file_rcu(const struct task_struct * task, const struct file * socket_file, const uint32_t packet_id)
{
  struct file  *  file = NULL;
  struct inode *  inode = NULL;
  int             fd_i = 0;
  int             fd_null = 0;
  int             fd_null_max = 0;
  bool            iterating_fd = true;

  if (task == NULL)
    return false;

  if (task->files == NULL)
    return false;

  rcu_read_lock();

  while (iterating_fd)
  {
    file = fcheck_files(task->files, fd_i);
    if (file)
    {
      /*
      ** In order to avoid going through all the fds of a process
      *  the fd_null variable is used to define a limit.
      *  This allows Douane to ignore processes which aren't owning the fd
      *  and switch to the next process, so Douane is faster.
      *
      *  But this limit can make Douane blind and will never find the process.
      *  In order to highligh this, the fd_null_max variable, which is printed
      *  in the logs in debug mode, shows the correct number for the current
      *  process.
      *
      *  For example, with Ubuntu <= 13.10, the correct value for fd_null is 3,
      *  while now with Ubuntu 14.04, the correct value for fd_null is around 8.
      */
      if (fd_null > fd_null_max)
        fd_null_max = fd_null;

      fd_null = 0;

      inode = file_inode(file);

      if (S_ISSOCK(inode->i_mode))
      {
        if (file == socket_file)
        {
          pr_debug("douane(%u):%d:%s: fd_null_max: %d | MAX_FD_NULL: %d\n", packet_id, __LINE__, __FUNCTION__, fd_null_max, MAX_FD_NULL);

          rcu_read_unlock();

          return true;
        }
      }
    } else {
      fd_null++;

      if (fd_null >= MAX_FD_NULL)
        iterating_fd = false;
    }

    fd_i++;
  }

  rcu_read_unlock();

  return false;
}

static char * task_exe_path_rcu(struct task_struct * task, char * buff, const uint32_t packet_id)
{
  struct task_struct * locked_task = task;
  char               * pathbuf,
                     * p;
  int                  deleted_position;

  if (task == NULL)
    return NULL;

  rcu_read_lock();

  pathbuf = kmalloc(PATH_MAX+10, GFP_KERNEL);

  do
  {
    task_lock(locked_task);

    if (likely(locked_task->mm) && locked_task->mm->exe_file)
    {
      // Get process path using d_path()
      // d_path() suffix the path with " (deleted)" when the file is still accessed
      // and the deletion of the file has been requested.

      p = NULL;
      if (pathbuf)
      {
        // The result of d_path is maximum PATH_MAX with " (deleted)" string
        p = d_path(&locked_task->mm->exe_file->f_path, pathbuf, PATH_MAX+10);
        if (IS_ERR(p))
        {
          p = NULL;
          pr_debug("douane(%u):%d:%s: d_path return ERROR\n", packet_id, __LINE__, __FUNCTION__);
        } else {

          strcpy(buff, p);

          task_unlock(locked_task);

          // Remove the deleted suffix if present
          if ((deleted_position = index_of(buff, " (deleted)")) > 0)
          {
            // Clean temp buffer in order to reuse it
            memset(&pathbuf[0], 0, PATH_MAX+10);
            // Copy the current buffer in the temp buffer
            // Truncate PATH_MAX to PATH_LENGTH
            // TODO : send the full path length to douane service
            strncpy(pathbuf, buff, PATH_LENGTH);
            // Clean the current buffer
            memset(&pathbuf[0], 0, PATH_LENGTH);
            // Copy the path without the " (deleted)" suffix
            strncpy(buff, pathbuf, deleted_position);
          }

          break;
        }
      } else
        pr_err("douane(%u):%d:%s: Cannot allocate pathbuf\n", packet_id, __LINE__, __FUNCTION__);
    }

    task_unlock(locked_task);

  } while_each_thread(task, locked_task);

  kfree(pathbuf);

  rcu_read_unlock();

  return buff;
}

static struct task_struct * task_from_socket_file_rcu(struct file * socket_file, const uint32_t packet_id)
{
  struct task_struct *  task;

  // https://www.kernel.org/doc/html/latest/RCU/listRCU.html
  rcu_read_lock();
  for_each_process(task)
  {
    rcu_read_unlock();

    get_task_struct(task);

    if (task_owns_socket_file_rcu(task, socket_file, packet_id))
      return task;

    rcu_read_lock();
  }
  rcu_read_unlock();

  return NULL;
}

static void print_tcp_packet(const struct tcphdr * tcp_header, const struct iphdr * ip_header, char * ip_source, char * ip_destination, char * process_path, const uint32_t packet_id)
{
  if (tcp_header)
  {
    pr_debug("douane(%u):%d:%s: TCP | [seq:%u|ack_seq:%u]\tFLAGS=%c%c%c%c%c%c | TGID %d:\t%s:%hu --> %s:%hu (%s|%s) {W:%hu|%d} %s.\n", packet_id, __LINE__, __FUNCTION__,
      ntohl(tcp_header->seq),
      ntohl(tcp_header->ack_seq),
      tcp_header->urg ? 'U' : '-',
      tcp_header->ack ? 'A' : '-',
      tcp_header->psh ? 'P' : '-',
      tcp_header->rst ? 'R' : '-',
      tcp_header->syn ? 'S' : '-',
      tcp_header->fin ? 'F' : '-',
      current->tgid,
      ip_source,
      ntohs(tcp_header->source),
      ip_destination,
      ntohs(tcp_header->dest),
      process_path,
      current->comm,
      ntohs(tcp_header->window),
      ntohs(ip_header->tot_len) - (tcp_header->doff * 4) - (ip_header->ihl * 4),
      "NF_ACCEPT"
    );
  }
}

static void print_udp_packet(const struct udphdr * udp_header, const struct iphdr * ip_header, char * ip_source, char * ip_destination, char * process_path,  const uint32_t packet_id)
{
  if (udp_header)
  {
    pr_debug("douane(%u):%d:%s: UDP | TGID %d:\t%s:%hu --> %s:%hu (%s|%s) %s.\n", packet_id, __LINE__, __FUNCTION__,
      current->tgid,
      ip_source,
      ntohs(udp_header->source),
      ip_destination,
      ntohs(udp_header->dest),
      process_path,
      current->comm,
      "NF_ACCEPT"
    );
  }
}

static char * ip_protocol_name_from(const int protocol)
{
  switch(protocol)
  {
    case IPPROTO_ICMP: return "ICMP";
    case IPPROTO_IGMP: return "IGMP";
    case IPPROTO_IPIP: return "IPIP";
    case IPPROTO_TCP: return "TCP";
    case IPPROTO_EGP: return "EGP";
    case IPPROTO_PUP: return "PUP";
    case IPPROTO_UDP: return "UDP";
    case IPPROTO_IDP: return "IDP";
    case IPPROTO_TP: return "TP";
    case IPPROTO_DCCP: return "DCCP";
    case IPPROTO_IPV6: return "IPV6";
    case IPPROTO_RSVP: return "RSVP";
    case IPPROTO_GRE: return "GRE";
    case IPPROTO_ESP: return "ESP";
    case IPPROTO_AH: return "AH";
    case IPPROTO_MTP: return "MTP";
    case IPPROTO_BEETPH: return "BEETPH";
    case IPPROTO_ENCAP: return "ENCAP";
    case IPPROTO_PIM: return "PIM";
    case IPPROTO_COMP: return "COMP";
    case IPPROTO_SCTP: return "SCTP";
    case IPPROTO_UDPLITE: return "UDPLITE";
    case IPPROTO_MPLS: return "MPLS";
    case IPPROTO_RAW: return "RAW";
    default: return "UNKNOWN";
  }
}

static char * lookup_from_socket_sequence_rcu(const struct tcphdr * tcp_header, char * buffer, const uint32_t packet_id)
{
  struct process_socket_inode * psi = NULL;

  if (tcp_header == NULL)
  {
    pr_err("douane(%u):%d:%s: !!OOPS!! Not a TCP packet! !!OOPS!!\n", packet_id, __LINE__, __FUNCTION__);

    return "";
  }

  psi = psi_from_sequence_rcu(ntohl(tcp_header->seq), packet_id);
  if (psi == NULL)
  {
    pr_err("douane(%u):%d:%s: !!OOPS!! skb->sk->sk_socket or skb->sk is NULL and nothing found from sequence %u. !!OOPS!!\n", packet_id, __LINE__, __FUNCTION__, ntohl(tcp_header->seq));

    return "";
  }

  strcpy(buffer, psi->process_path);

  kfree_rcu(psi, rcu);

  return buffer;
}

/*
**  Process informations
*
*   Using the socket file we're going through each running processes
*   and compare the socket file to the process opened files.
*   Creating a socket in the userspace create a file that is added
*   in the list of process opened files (On Linux everything is a file).
*/
static char * find_process_owner_path_from_socket_rcu(const struct sk_buff * skb, const struct tcphdr * tcp_header, char * buffer, bool * filterable, const uint32_t packet_id)
{
  struct process_socket_inode * psi = NULL;
  struct pid *                  task_pid_struct = NULL;
  struct task_struct *          task = NULL;
  struct task_struct *          task_struct_owning_socket_file = NULL;
  char                          task_exe_buffer[PATH_LENGTH] = "";
  bool                          task_owning_socket_file = false;
  unsigned long                 socket_file_ino;

  pr_debug("douane(%u):%d:%s: Checking skb->sk ...\n", packet_id, __LINE__, __FUNCTION__);

  if (!skb->sk)
  {
    pr_debug("douane(%u):%d:%s: !!WARN WARN WARN!! skb->sk is NULL. !!WARN WARN WARN!!\n", packet_id, __LINE__, __FUNCTION__);

    strcpy(buffer, lookup_from_socket_sequence_rcu(tcp_header, task_exe_buffer, packet_id));

    pr_debug("douane(%u):%d:%s: !!WARN WARN WARN!! returning '%s'. !!WARN WARN WARN!!\n", packet_id, __LINE__, __FUNCTION__, buffer);
    return buffer;
  }

  pr_debug("douane(%u):%d:%s: read_lock_bh ...\n", packet_id, __LINE__, __FUNCTION__);
  read_lock_bh(&skb->sk->sk_callback_lock);
  pr_debug("douane(%u):%d:%s: read_lock_bh OK!\n", packet_id, __LINE__, __FUNCTION__);

  pr_debug("douane(%u):%d:%s: rcu_read_lock ...\n", packet_id, __LINE__, __FUNCTION__);
  rcu_read_lock();
  pr_debug("douane(%u):%d:%s: rcu_read_lock OK!\n", packet_id, __LINE__, __FUNCTION__);
  pr_debug("douane(%u):%d:%s: Checking skb->sk->sk_socket and skb->sk->sk_socket->file ...\n", packet_id, __LINE__, __FUNCTION__);

  if (skb->sk->sk_socket && skb->sk->sk_socket->file)
  {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,9,0)
    socket_file_ino = skb->sk->sk_socket->file->f_inode->i_ino;
#else
    socket_file_ino = skb->sk->sk_socket->file->f_dentry->d_inode->i_ino;
#endif
  } else {
    pr_debug("douane(%u):%d:%s: rcu_read_unlock ...\n", packet_id, __LINE__, __FUNCTION__);
    rcu_read_unlock();
    pr_debug("douane(%u):%d:%s: rcu_read_unlock OK!\n", packet_id, __LINE__, __FUNCTION__);

    pr_debug("douane(%u):%d:%s: read_unlock_bh ...\n", packet_id, __LINE__, __FUNCTION__);
    read_unlock_bh(&skb->sk->sk_callback_lock);
    pr_debug("douane(%u):%d:%s: read_unlock_bh OK!\n", packet_id, __LINE__, __FUNCTION__);

    pr_debug("douane(%u):%d:%s: !!WARN WARN WARN!! skb->sk->sk_socket is NULL. !!WARN WARN WARN!!\n", packet_id, __LINE__, __FUNCTION__);

    strcpy(buffer, lookup_from_socket_sequence_rcu(tcp_header, task_exe_buffer, packet_id));

    pr_debug("douane(%u):%d:%s: !!WARN WARN WARN!! returning '%s'. !!WARN WARN WARN!!\n", packet_id, __LINE__, __FUNCTION__, buffer);
    return buffer;
  }

  pr_debug("douane(%u):%d:%s: rcu_read_unlock ...\n", packet_id, __LINE__, __FUNCTION__);
  rcu_read_unlock();
  pr_debug("douane(%u):%d:%s: rcu_read_unlock OK!\n", packet_id, __LINE__, __FUNCTION__);

  pr_debug("douane(%u):%d:%s: read_unlock_bh ...\n", packet_id, __LINE__, __FUNCTION__);
  read_unlock_bh(&skb->sk->sk_callback_lock);
  pr_debug("douane(%u):%d:%s: read_unlock_bh OK!\n", packet_id, __LINE__, __FUNCTION__);

  /*
  ** The current packet is filterable as we have the socket file
  */
  *filterable = true;

  pr_debug("douane(%u):%d:%s: filterable is now true\n", packet_id, __LINE__, __FUNCTION__);
  pr_debug("douane(%u):%d:%s: Working with ino %ld ...\n", packet_id, __LINE__, __FUNCTION__, socket_file_ino);

  /*
  ** Retrives a previously saved process_socket_inode instance for the
  *  given socket file inode.
  */
  psi = psi_from_inode_rcu(socket_file_ino, packet_id);
  if (psi == NULL)
  {
    // pr_info("douane(%u):%d:%s: rcu_read_lock ...\n", packet_id, __LINE__, __FUNCTION__);
    // read_lock_bh(&skb->sk->sk_callback_lock);
    // rcu_read_lock();
    // pr_info("douane(%u):%d:%s: rcu_read_lock OK!\n", packet_id, __LINE__, __FUNCTION__);

    task_struct_owning_socket_file = task_from_socket_file_rcu(skb->sk->sk_socket->file, packet_id);

    // pr_info("douane(%u):%d:%s: rcu_read_unlock ...\n", packet_id, __LINE__, __FUNCTION__);
    // read_unlock_bh(&skb->sk->sk_callback_lock);
    // rcu_read_unlock();
    // pr_info("douane(%u):%d:%s: rcu_read_unlock OK!\n", packet_id, __LINE__, __FUNCTION__);

    if (task_struct_owning_socket_file)
    {
      strcpy(buffer, task_exe_path_rcu(task_struct_owning_socket_file, task_exe_buffer, packet_id));
      pr_debug("douane(%u):%d:%s: task_exe_path_rcu returned '%s'\n", packet_id, __LINE__, __FUNCTION__, buffer);

      if (tcp_header)
      {
        // Remember relation task <-> socket file
        psi_remember_rcu(task_struct_owning_socket_file->pid, ntohl(tcp_header->seq), socket_file_ino, buffer, packet_id);
      } else {
        // Remember relation task <-> socket file
        psi_remember_rcu(task_struct_owning_socket_file->pid, 0, socket_file_ino, buffer, packet_id);
      }

      pr_debug("douane(%u):%d:%s: returning '%s'\n", packet_id, __LINE__, __FUNCTION__, buffer);
      return buffer;
    } else {
      pr_debug("douane(%u):%d:%s: !!OOPS!! Can't find a task for socket file inode number %ld !!OOPS!!\n", packet_id, __LINE__, __FUNCTION__, socket_file_ino);

      return "";
    }
  }

  /*
  ** In the case of TCP packets, this module follows its sequence so
  *  that it can avoid searching the emiter of the packet, so the
  *  process_socket_inode instance sequence is updated.
  */
  if (tcp_header)
    psi_update_sequence_rcu(socket_file_ino, ntohl(tcp_header->seq), packet_id);

  if (!psi->pid)
  {
    kfree_rcu(psi, rcu);
    pr_debug("douane(%u):%d:%s: !!OOO!! PSI has no PID. !!OOO!!\n", packet_id, __LINE__, __FUNCTION__);
    return "";
  }

  pr_debug("douane(%u):%d:%s: Looking for pid struct for PID %d ...\n", packet_id, __LINE__, __FUNCTION__, psi->pid);

  if (current->thread_pid) {
    // find the pid structure from the pid of a process
    task_pid_struct = find_get_pid(psi->pid);
  } else {
    kfree_rcu(psi, rcu);
    pr_debug("douane(%u):%d:%s: Cannot fetch the task's pid struct for PID %d: current thread_pid is NULL!\n", packet_id, __LINE__, __FUNCTION__, psi->pid);

    return "";
  }

  if (task_pid_struct == NULL)
  {
    kfree_rcu(psi, rcu);
    pr_debug("douane(%u):%d:%s: !!OOO!! PID struct not found for PID %d !!OOO!!\n", packet_id, __LINE__, __FUNCTION__, psi->pid);

    return "";
  }

  pr_debug("douane(%u):%d:%s: pid struct found from PID %d. Getting the task ...\n", packet_id, __LINE__, __FUNCTION__, psi->pid);
  task = get_pid_task(task_pid_struct, PIDTYPE_PID);
  if (task == NULL)
  {
    kfree_rcu(psi, rcu);
    pr_debug("douane(%u):%d:%s: !!OOO!! Task no more exists. !!OOO!!\n", packet_id, __LINE__, __FUNCTION__);

    return "";
  }

  pr_debug("douane(%u):%d:%s: task found from PID %d. Looking for opened files from that tasks ...\n", packet_id, __LINE__, __FUNCTION__, psi->pid);
  task_owning_socket_file = task_owns_socket_file_rcu(task, skb->sk->sk_socket->file, packet_id);

  if (task_owning_socket_file)
  {
    pr_debug("douane(%u):%d:%s: Task with PID %d owns the socket file, saving path %s ...\n", packet_id, __LINE__, __FUNCTION__, psi->pid, psi->process_path);
    strcpy(buffer, psi->process_path);
    kfree_rcu(psi, rcu);

    return buffer;
  } else {
    pr_debug("douane(%u):%d:%s: Task with PID %d doesn't own the socket file, checking headers ...\n", packet_id, __LINE__, __FUNCTION__, psi->pid);

    if (tcp_header == NULL) {
      pr_debug("douane(%u):%d:%s: Task with PID %d is sending a non TCP packet\n", packet_id, __LINE__, __FUNCTION__, psi->pid);
    } else {
      if (tcp_header && tcp_header->fin == false) {
        pr_debug("douane(%u):%d:%s: Task with PID %d is sending a TCP packet not marked as FIN\n", packet_id, __LINE__, __FUNCTION__, psi->pid);
      }
    }

    // For all non TCP packet and for TCP packet (except packets flaged as FIN)
    if (tcp_header == NULL || (tcp_header && tcp_header->fin == false))
    {
      pr_debug("douane(%u):%d:%s: Searching task owning the socket from PID %d ...\n", packet_id, __LINE__, __FUNCTION__, psi->pid);

      pr_debug("douane(%u):%d:%s: Aquiring the skb->sk->sk_callback_lock lock ...\n", packet_id, __LINE__, __FUNCTION__);
      read_lock_bh(&skb->sk->sk_callback_lock);
      pr_debug("douane(%u):%d:%s: Lock skb->sk->sk_callback_lock aquired!\n", packet_id, __LINE__, __FUNCTION__);

      task_struct_owning_socket_file = task_from_socket_file_rcu(skb->sk->sk_socket->file, packet_id);

      pr_debug("douane(%u):%d:%s: Releasing the skb->sk->sk_callback_lock lock ...\n", packet_id, __LINE__, __FUNCTION__);
      read_unlock_bh(&skb->sk->sk_callback_lock);
      pr_debug("douane(%u):%d:%s: skb->sk->sk_callback_lock lock released!\n", packet_id, __LINE__, __FUNCTION__);

      if (task_struct_owning_socket_file)
      {
        pr_debug("douane(%u):%d:%s: Task with opened socket found with PID %d\n", packet_id, __LINE__, __FUNCTION__, psi->pid);

        /*
        ** The task is no more owning the passed socket file so
        *  we can forget the relation task <-> socket file.
        */
        psi_forget_rcu(task->pid, socket_file_ino, packet_id);

        strcpy(buffer, task_exe_path_rcu(task_struct_owning_socket_file, task_exe_buffer, packet_id));

        if (tcp_header)
        {
          // Remember relation task <-> socket file
          psi_remember_rcu(task_struct_owning_socket_file->pid, ntohl(tcp_header->seq), socket_file_ino, buffer, packet_id);
        } else {
          // Remember relation task <-> socket file
          psi_remember_rcu(task_struct_owning_socket_file->pid, 0, socket_file_ino, buffer, packet_id);
        }

        kfree_rcu(psi, rcu);
        return buffer;
      } else {
        pr_debug("douane(%u):%d:%s: Task with PID %d is sending a ACK packet after a FIN, copying process path %s ...\n", packet_id, __LINE__, __FUNCTION__, psi->pid, psi->process_path);

        // The current packet is a ACK packet after a FIN
        strcpy(buffer, psi->process_path);
        kfree_rcu(psi, rcu);
        return buffer;
      }
    // Only for TCP packets flaged as FIN
    } else {
      pr_debug("douane(%u):%d:%s: Task with PID %d is sending a TCP packet flaged as FIN, copying process path %s ...\n", packet_id, __LINE__, __FUNCTION__, psi->pid, psi->process_path);

      strcpy(buffer, psi->process_path);
      kfree_rcu(psi, rcu);
      return buffer;
    }
  }

  kfree_rcu(psi, rcu);
  return "";
}

/*
**  Netfiler hook
*/
#if LINUX_VERSION_CODE < KERNEL_VERSION(3,13,0)
static unsigned int netfiler_packet_hook(unsigned int hook, struct sk_buff *skb, const struct net_device *in, const struct net_device *out, int (*okfn)(struct sk_buff *skb))
#elif LINUX_VERSION_CODE < KERNEL_VERSION(4,1,0)
static unsigned int netfiler_packet_hook(const struct nf_hook_ops *ops, struct sk_buff *skb, const struct net_device *in, const struct net_device *out, int (*okfn)(struct sk_buff *skb))
#elif LINUX_VERSION_CODE < KERNEL_VERSION(4,4,0)
static unsigned int netfiler_packet_hook(const struct nf_hook_ops *ops, struct sk_buff *skb, const struct nf_hook_state *state)
#else
static unsigned int netfiler_packet_hook(void *priv, struct sk_buff *skb, const struct nf_hook_state *state)
#endif
{
  struct iphdr *                ip_header = NULL;
  struct udphdr *               udp_header = NULL;
  struct tcphdr *               tcp_header = NULL;
  struct network_activity *     activity = (struct network_activity*) kmalloc(sizeof(struct network_activity), GFP_ATOMIC);
  struct rule *                 rule = NULL;
  char                          ip_source[16];
  char                          ip_destination[16];
  char                          process_owner_path[PATH_LENGTH] = "";
  char                          buffer[PATH_LENGTH] = "";
  int                           sport = 0;
  int                           dport = 0;
  bool                          filterable = false;
  bool                          known_protocol = false;
  uint32_t                      packet_id;

  // Generates a packet ID used everywhere in order to have a way to group logs
  // together.
  get_random_bytes(&packet_id, sizeof(packet_id));

  // This marker is useful in order to find the begining of a flow
  pr_debug("douane(%u):%d:%s: ~~~~~~~~~~~~~ Treating a new network packet ~~~~~~~~~~~~~\n", packet_id, __LINE__, __FUNCTION__);

  if (activity == NULL)
  {
    pr_err("douane(%u):%d:%s: Can't allocate memory for a new activity\n", packet_id, __LINE__, __FUNCTION__);
    return -ENOMEM;
  }

  if (stopping) {
    pr_debug("douane(%u):%d:%s: NF_ACCEPT as the module is stopping\n", packet_id, __LINE__, __FUNCTION__);
    kfree(activity);
    return NF_ACCEPT;
  }

  // Empty socket buffer
  if (skb == NULL)
  {
    pr_warn("douane(%u):%d:%s: Allowing traffic as socket buffer is NULL.\n", packet_id, __LINE__, __FUNCTION__);
    kfree(activity);
    return NF_ACCEPT;
  }

  /*
  **  Network informations
  *
  *  Using IP/TCP/UDP headers find source and destination IP addresses
  *  ports, size etc...
  */
  // TODO : Move all that code into a dedicated function
  // Retrieve IP information (Source and Destination IP/port)
  ip_header = ip_hdr(skb);
  if (ip_header == NULL)
  {
    pr_debug("douane(%u):%d:%s: !!OOPS!! ip_header is NULL. !!OOPS!!\n", packet_id, __LINE__, __FUNCTION__);
    kfree(activity);
    return NF_ACCEPT;
  }

  // Convert to string IP addresses
  snprintf(ip_source, 16, "%pI4", &ip_header->saddr);
  snprintf(ip_destination, 16, "%pI4", &ip_header->daddr);

  // Based on the packet protocol
  switch(ip_header->protocol)
  {
    // UDP
    case IPPROTO_UDP:
    {
      // Getting UDP header
      udp_header = udp_hdr(skb);
      if (udp_header == NULL)
      {
        pr_debug("douane(%u):%d:%s: !!OOPS!! udp_header is NULL. !!OOPS!!\n", packet_id, __LINE__, __FUNCTION__);
	      kfree(activity);
        return NF_ACCEPT;
      }
      // Getting source and destination ports
      sport = (unsigned int) ntohs(udp_header->source);
      dport = (unsigned int) ntohs(udp_header->dest);

      known_protocol = true;
      break;
    }

    // TCP
    case IPPROTO_TCP:
    {
      // Getting TCP header
      tcp_header = tcp_hdr(skb);
      if (tcp_header == NULL)
      {
        pr_debug("douane(%u):%d:%s: !!OOPS!! tcp_header is NULL. !!OOPS!!\n", packet_id, __LINE__, __FUNCTION__);
	      kfree(activity);
        return NF_ACCEPT;
      }
      // Getting source and destination ports
      sport = (unsigned int) ntohs(tcp_header->source);
      dport = (unsigned int) ntohs(tcp_header->dest);

      known_protocol = true;
      break;
    }

    // All others
    default:
    {
      pr_debug("douane(%u):%d:%s: [UNMANAGED(%s [%d])] %s -> %s\n", packet_id, __LINE__, __FUNCTION__,
        ip_protocol_name_from(ip_header->protocol),
        ip_header->protocol,
        ip_source,
        ip_destination);

      kfree(activity);
      return NF_ACCEPT;
    }
  }

  // Retrieves the socket file owner
  strcpy(process_owner_path, find_process_owner_path_from_socket_rcu(skb, tcp_header, buffer, &filterable, packet_id));

  // Log the packet
  // TODO : Extract this code in a function
  switch(ip_header->protocol)
  {
    case IPPROTO_UDP: {
      print_udp_packet(udp_header, ip_header, ip_source, ip_destination, process_owner_path, packet_id);
      break;
    }
    case IPPROTO_TCP: {
      print_tcp_packet(tcp_header, ip_header, ip_source, ip_destination, process_owner_path, packet_id);
      break;
    }
  }

  // When was filterable but didn't got process path
  if (filterable && (process_owner_path == NULL || strcmp(process_owner_path, "") == 0))
  {
    kfree(activity);

    pr_debug("douane(%u):%d:%s: !!OOPS!! process_owner_path is blank but was filterable! !!OOPS!!\n", packet_id, __LINE__, __FUNCTION__);
    pr_debug("douane(%u):%d:%s: NF_ACCEPT (No process path)\n", packet_id, __LINE__, __FUNCTION__);

    return NF_ACCEPT;
  }

  /*
  **  Accept the packet if:
  *  - daemon not connected
  *  - daemon process has PID 0
  */
  // TODO : Extract all that code in a function which returns the final NF_
  if (dnl_activities_socket == NULL || dnl_daemon_pid == 0)
  {
    kfree(activity);

    pr_debug("douane(%u):%d:%s: NF_ACCEPT (No daemon)\n", packet_id, __LINE__, __FUNCTION__);

    return NF_ACCEPT;
  }

  pr_debug("douane(%u):%d:%s: Preparing activity ...\n", packet_id, __LINE__, __FUNCTION__);

  /*
  **  Building new network_activity message
  */
  strcpy(activity->process_path, process_owner_path);
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,1,0)
  strcpy(activity->devise_name, out->name);
#else
  strcpy(activity->devise_name, state->out->name);
#endif
  activity->protocol = ip_header->protocol;
  strcpy(activity->ip_source, ip_source);
  activity->port_source = sport;
  strcpy(activity->ip_destination, ip_destination);
  activity->port_destination = dport;
  activity->size = skb->len;

  // Push the activity to the daemon process
  if (dnl_notify_douane_daemon(activity, packet_id) < 0) {
    pr_err("douane(%u):%d:%s: Something prevent to sent the network activity\n", packet_id, __LINE__, __FUNCTION__);
  } else {
    pr_debug("douane(%u):%d:%s: Activity pushed to the daemon with PID %d\n", packet_id, __LINE__, __FUNCTION__, dnl_daemon_pid);
  }

  kfree(activity);

  if (filterable)
  {
    pr_debug("douane(%u):%d:%s: Searching rule for %s ...\n", packet_id, __LINE__, __FUNCTION__, process_owner_path);
    rule = rules_search_rcu(process_owner_path, packet_id);

    if (rule == NULL)
    {
      pr_debug("douane(%u):%d:%s: NF_QUEUE for %s as no rules_list yet\n", packet_id, __LINE__, __FUNCTION__, process_owner_path);

      return NF_QUEUE;
    } else {
      if (rule->allowed)
      {
        pr_debug("douane(%u):%d:%s: NF_ACCEPT for %s as allowed\n", packet_id, __LINE__, __FUNCTION__, process_owner_path);

        return NF_ACCEPT;
      } else {
        pr_debug("douane(%u):%d:%s: NF_DROP for %s as disallowed\n", packet_id, __LINE__, __FUNCTION__, process_owner_path);

        return NF_DROP;
      }
    }
  } else {
    pr_debug("douane(%u):%d:%s: NF_ACCEPT for %s as not filterable\n", packet_id, __LINE__, __FUNCTION__, process_owner_path);

    return NF_ACCEPT;
  }
}


/*
**  Netfiler hook
*/
static struct nf_hook_ops nfho_outgoing = {
  .hook     = netfiler_packet_hook,
  .hooknum  = NF_IP_LOCAL_OUT,
  .pf       = NFPROTO_IPV4,
  .priority = NF_IP_PRI_LAST,
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,4,0)
  .owner    = THIS_MODULE
#endif
};


/*
** Linux kernel module initializer and cleaner methods
*/
static int __init initialize_module(void)
{
#ifdef DEBUG
  struct net_device *dev;

  pr_debug("douane:%d:%s: Initializing module\n", __LINE__, __FUNCTION__);

  // Net Device debugging
  dev = first_net_device(&init_net);
  while(dev)
  {
    pr_debug("douane:%d:%s: Net_Device found: name: %s - ifindex: %d\n", __LINE__, __FUNCTION__, dev->name, dev->ifindex);
    dev = next_net_device(dev);
  }
#endif

  // Hook to Netfilter
#if LINUX_VERSION_CODE <= KERNEL_VERSION(4,12,14)
  nf_register_hook(&nfho_outgoing);
#else
  nf_register_net_hook(&init_net, &nfho_outgoing);
#endif

  // Open a Netfilter socket to communicate with the user space
  if (dnl_initialize_socket(activities_socket_receiver) < 0)
  {
    pr_err("douane:%d:%s: Unable to create Netlink socket!\n", __LINE__, __FUNCTION__);
    return -1;
  }

#ifdef DEBUG
  pr_debug("douane:%d:%s: Kernel module is ready!\n", __LINE__, __FUNCTION__);
#else
  pr_info("douane: Kernel module loaded\n");
#endif
  return 0;
}
static void __exit exit_module(void)
{
#if LINUX_VERSION_CODE <= KERNEL_VERSION(4,12,14)
  nf_unregister_hook(&nfho_outgoing);
#else
  nf_unregister_net_hook(&init_net, &nfho_outgoing);
#endif

  dnl_shutdown_socket();
  blackout();

#ifdef DEBUG
  pr_debug("douane:%d:%s: Kernel module removed!\n", __LINE__, __FUNCTION__);
#else
  pr_info("douane: Kernel module unloaded\n");
#endif
}

// Boot the Douane Linux Kernel Module
module_init(initialize_module);
module_exit(exit_module);
