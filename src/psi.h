#ifndef _PSI_H_
#define _PSI_H_

/*
**  Process socket inode struct
*
*   This struct is a linked list which is used by Douane in order to remember
*   process pathes in order to speed-up finding socket buffer owner and also
*   used to follow TCP socket sequence frames.
*/
struct process_socket_inode
{
  unsigned long     i_ino;                     // Process socket file inode
  pid_t             pid;                       // PID of the process
  char              process_path[PATH_LENGTH]; // Path of the process
  uint32_t          sequence;                  // TCP sequence (Is be 0 for non TCP packets)
  struct list_head  list;
  struct rcu_head   rcu;
};

struct process_socket_inode * psi_from_inode_rcu(const unsigned long i_ino, const uint32_t packet_id);
struct process_socket_inode * psi_from_pid_rcu(const pid_t pid, const uint32_t packet_id);
struct process_socket_inode * psi_from_sequence_rcu(const uint32_t sequence, const uint32_t packet_id);
void psi_forget_rcu(const pid_t pid, const unsigned long i_ino, const uint32_t packet_id);
void psi_clear_rcu(void);
void psi_remember_rcu(const pid_t pid, const uint32_t sequence, const unsigned long i_ino, const char * path, const uint32_t packet_id);
void psi_update_sequence_rcu(const unsigned long i_ino, const uint32_t sequence, const uint32_t packet_id);

#endif // _PSI_H_
