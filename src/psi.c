// Douane kernel module
// Copyright (C) 2013  Guillaume Hain <zedtux@zedroot.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <linux/module.h>         // Needed by all modules
#include <linux/kernel.h>         // Needed for KERN_INFO
#include <linux/version.h>        // Needed for LINUX_VERSION_CODE >= KERNEL_VERSION
// ~~~~ Due to bug https://bugs.launchpad.net/ubuntu/+source/linux/+bug/929715 ~~~~
// #undef __KERNEL__
// #include <linux/netfilter_ipv4.h> // NF_IP_POST_ROUTING, NF_IP_PRI_LAST
// #define __KERNEL__
#define NF_IP_LOCAL_OUT 3
enum nf_ip_hook_priorities {
  NF_IP_PRI_LAST = INT_MAX
};
// ~~~~
#include <linux/netdevice.h>	    // net_device
#include <linux/netfilter.h>      // nf_register_hook(), nf_unregister_hook(), nf_register_net_hook(), nf_unregister_net_hook()
#include <linux/netlink.h>        // NLMSG_SPACE(), nlmsg_put(), NETLINK_CB(), NLMSG_DATA(), NLM_F_REQUEST, netlink_unicast(), netlink_kernel_release(), nlmsg_hdr(), NETLINK_USERSOCK, netlink_kernel_create()

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
#include <linux/signal.h>
#else
#include <linux/sched/signal.h>          // for_each_process(), task_lock(), task_unlock()
#endif

#include <linux/ip.h>             // ip_hdr()
#include <linux/udp.h>            // udp_hdr()
#include <linux/tcp.h>            // tcp_hdr()
#include <linux/fdtable.h>        // files_fdtable(), fcheck_files()
#include <linux/list.h>           // INIT_LIST_HEAD(), list_for_each_entry(), list_add_tail(), list_empty(), list_entry(), list_del(), list_for_each_entry_safe()
#include <linux/dcache.h>         // d_path()
#include <linux/skbuff.h>         // alloc_skb()
#include <linux/pid_namespace.h>  // task_active_pid_ns()
#include <linux/rculist.h>        // hlist_for_each_entry_rcu

#include "network_activity_message.h"

#include "psi.h"

DEFINE_SPINLOCK(psi_lock);
LIST_HEAD(psi_nodelist);

/*
** Search process_socket_inode functions
*/
// By socket file inode
struct process_socket_inode * psi_from_inode_rcu(const unsigned long i_ino, const uint32_t packet_id)
{
  struct process_socket_inode * psi;
  struct process_socket_inode * psi_to_return;

  if (i_ino == 0)
    return NULL;

  pr_debug("douane(%u):%d:%s: Searching inode from i_ino %lu ...\n", packet_id, __LINE__, __FUNCTION__, i_ino);

  rcu_read_lock();
  list_for_each_entry_rcu(psi, &psi_nodelist, list)
  {
    if (psi->i_ino == i_ino) {
      pr_debug("douane(%u):%d:%s: inode found for i_ino %lu, preparing a copy ...\n", packet_id, __LINE__, __FUNCTION__, i_ino);

      // Creating a copy that will be returned (RCU)
      psi_to_return = kzalloc(sizeof(struct process_socket_inode), GFP_ATOMIC);
      if (!psi_to_return) {
        rcu_read_unlock();
        pr_err("douane(%u):%d:%s: Failed to allocate new process_socket_inode.\n", packet_id, __LINE__, __FUNCTION__);
        return NULL;
      }

      pr_debug("douane(%u):%d:%s: memcpy psi into psi_to_return ...\n", packet_id, __LINE__, __FUNCTION__);
      memcpy(psi_to_return, psi, sizeof(struct process_socket_inode));

      rcu_read_unlock();

      pr_debug("douane(%u):%d:%s: returning psi copy ...\n", packet_id, __LINE__, __FUNCTION__);
      return psi_to_return;
    }
  }
  rcu_read_unlock();

  pr_debug("douane(%u):%d:%s: inode not found for i_ino %lu ...\n", packet_id, __LINE__, __FUNCTION__, i_ino);

  return NULL;
}

// By PID
struct process_socket_inode * psi_from_pid_rcu(const pid_t pid, const uint32_t packet_id)
{
  struct process_socket_inode * psi;
  struct process_socket_inode * psi_to_return;

  if (pid == 0)
    return NULL;

  pr_debug("douane(%u):%d:%s: Searching inode from pid %d ...\n", packet_id, __LINE__, __FUNCTION__, pid);

  rcu_read_lock();
  list_for_each_entry_rcu(psi, &psi_nodelist, list)
  {
    if (psi->pid == pid) {
      // Creating a copy that will be returned (RCU)
      psi_to_return = kzalloc(sizeof(struct process_socket_inode), GFP_ATOMIC);
      if (!psi_to_return) {
        rcu_read_unlock();
        pr_err("douane(%u):%d:%s: Failed to allocate new process_socket_inode.\n", packet_id, __LINE__, __FUNCTION__);
        return NULL;
      }

      pr_debug("douane(%u):%d:%s: memcpy psi into psi_to_return ...\n", packet_id, __LINE__, __FUNCTION__);
      memcpy(psi_to_return, psi, sizeof(struct process_socket_inode));

      rcu_read_unlock();

      pr_debug("douane(%u):%d:%s: inode found for pid %d ...\n", packet_id, __LINE__, __FUNCTION__, pid);
      return psi_to_return;
    }
  }
  rcu_read_unlock();

  pr_debug("douane(%u):%d:%s: inode not found for pid %d ...\n", packet_id, __LINE__, __FUNCTION__, pid);

  return NULL;
}

// By socket sequence
struct process_socket_inode * psi_from_sequence_rcu(const uint32_t sequence, const uint32_t packet_id)
{
  struct process_socket_inode * psi;
  struct process_socket_inode * psi_to_return;

  if (sequence == 0)
    return NULL;

  pr_debug("douane(%u):%d:%s: Searching inode from sequence %u ...\n", packet_id, __LINE__, __FUNCTION__, sequence);

  rcu_read_lock();
  list_for_each_entry_rcu(psi, &psi_nodelist, list)
  {
    if (psi->sequence > 0)
    {
      if (psi->sequence == sequence || ((psi->sequence + 1) == sequence))
      {
        // Creating a copy that will be returned (RCU)
        psi_to_return = kzalloc(sizeof(struct process_socket_inode), GFP_ATOMIC);
        if (!psi_to_return) {
          rcu_read_unlock();
          pr_err("douane(%u):%d:%s: Failed to allocate new process_socket_inode.\n", packet_id, __LINE__, __FUNCTION__);
          return NULL;
        }

        pr_debug("douane(%u):%d:%s: memcpy psi into psi_to_return ...\n", packet_id, __LINE__, __FUNCTION__);
        memcpy(psi_to_return, psi, sizeof(struct process_socket_inode));

        rcu_read_unlock();

        pr_debug("douane(%u):%d:%s: inode found for sequence %u ...\n", packet_id, __LINE__, __FUNCTION__, sequence);
        return psi_to_return;
      }
    }
  }
  rcu_read_unlock();

  pr_debug("douane(%u):%d:%s: inode not found for sequence %u ...\n", packet_id, __LINE__, __FUNCTION__, sequence);

  return NULL;
}

/*
** Delete functions
*/
void psi_forget_rcu(const pid_t pid, const unsigned long i_ino, const uint32_t packet_id)
{
  struct process_socket_inode * psi;

  if (pid == 0 || i_ino == 0)
    return;

  pr_debug("douane(%u):%d:%s: Forgetting saved process <-> socket file for PID %d and ino %lu ...\n", packet_id, __LINE__, __FUNCTION__, pid, i_ino);

  spin_lock(&psi_lock);
  list_for_each_entry(psi, &psi_nodelist, list)
  {
    if (psi->pid == pid && psi->i_ino == i_ino)
    {
      list_del_rcu(&psi->list);
      spin_unlock(&psi_lock);

      /*
      ** This module can't block, therefore it can't use synchronize_rcu() and
      *  kfree(), instead it uses kfree_rcu() which creates a callback function
      *  to free the given list entry while in a grace period.
      */
      kfree_rcu(psi, rcu);

      pr_debug("douane(%u):%d:%s: Saved process <-> socket file for PID %d and ino %lu successfully deleted\n", packet_id, __LINE__, __FUNCTION__, pid, i_ino);
      return;
    }
  }
  spin_unlock(&psi_lock);

  pr_debug("douane(%u):%d:%s: No saved process <-> socket file for PID %d and ino %lu deleted\n", packet_id, __LINE__, __FUNCTION__, pid, i_ino);
}

// This function is called when something is wrong or when the module is about
// to be unloaded, therefore we set the module to stop processing packets
// (and therefore stopping accessing the psi_nodelist list).
void psi_clear_rcu(void)
{
  struct process_socket_inode * psi;
  int                           process_cleaned_records = 0;

  if (list_empty(&psi_nodelist))
    return;

  pr_debug("douane:%d:%s: Clearing psi_nodelist ...\n", __LINE__, __FUNCTION__);

  spin_lock(&psi_lock);
  list_for_each_entry(psi, &psi_nodelist, list)
  {
    list_del_rcu(&psi->list);

    synchronize_rcu();
    kfree(psi);

    process_cleaned_records++;
  }
  spin_unlock(&psi_lock);

  pr_debug("douane:%d:%s: %d relations process <-> socket file successfully cleaned.\n", __LINE__, __FUNCTION__, process_cleaned_records);
}

/*
** Add functions
*/

void psi_remember_rcu(const pid_t pid, const uint32_t sequence, const unsigned long i_ino, const char * path, const uint32_t packet_id)
{
  struct process_socket_inode * new_process_socket_inode = NULL;
  struct process_socket_inode * psi = NULL;

  if (pid == 0 || i_ino == 0 || path == NULL)
    return;

  if (strlen(path) > PATH_LENGTH)
    return;

  pr_debug("douane(%u):%d:%s: Checking previously stored process socket mapping ...\n", packet_id, __LINE__, __FUNCTION__);

  /*
  ** In order to cleanup this linked list, before saving the new relation
  *  process <-> socket file, we first look if there is an entry for the given
  *  PID but with a different path, and then we delete it.
  */
  psi = psi_from_pid_rcu(pid, packet_id);

  rcu_read_lock();
  if (psi) {
    if (strncmp(psi->process_path, path, PATH_LENGTH) != 0) {
      pr_debug("douane(%u):%d:%s: A psi_nodelist found for the PID %d but with a different path, deleting it ...\n", packet_id, __LINE__, __FUNCTION__, pid);
      psi_forget_rcu(pid, i_ino, packet_id);
    }
  }
  rcu_read_unlock();

  if (psi) {
    kfree_rcu(psi, rcu);
  }

  pr_debug("douane(%u):%d:%s: Saving the new process <-> socket file relation ...\n", packet_id, __LINE__, __FUNCTION__);

  /*
  ** Now save the new entry for the new application
  */
  new_process_socket_inode = (struct process_socket_inode *)kmalloc(sizeof(struct process_socket_inode), GFP_ATOMIC);
  if(new_process_socket_inode == NULL)
  {
    pr_err("douane(%u):%d:%s: Failed to allocate new process_socket_inode.\n", packet_id, __LINE__, __FUNCTION__);
    return;
  }

  new_process_socket_inode->i_ino = i_ino;
  new_process_socket_inode->pid = pid;
  new_process_socket_inode->sequence = sequence;
  strncpy(new_process_socket_inode->process_path, path, PATH_LENGTH);

  pr_debug("douane(%u):%d:%s: Saving new process_socket_inode with i_ino %lu, pid %d, sequence: %u, and path %s ...\n", packet_id, __LINE__, __FUNCTION__, new_process_socket_inode->i_ino, new_process_socket_inode->pid, new_process_socket_inode->sequence, new_process_socket_inode->process_path);

  spin_lock(&psi_lock);
  list_add_tail_rcu(&new_process_socket_inode->list, &psi_nodelist);
  spin_unlock(&psi_lock);
}

/*
** In-place update functions
*/

void psi_update_sequence_rcu(const unsigned long i_ino, const uint32_t sequence, const uint32_t packet_id)
{
  struct process_socket_inode * old_psi = NULL;
  struct process_socket_inode * new_psi = NULL;

  if (i_ino == 0)
    return;

  pr_debug("douane(%u):%d:%s: Searching old psi from inode %lu ...\n", packet_id, __LINE__, __FUNCTION__, i_ino);
  old_psi = psi_from_inode_rcu(i_ino, packet_id);

  if (old_psi == NULL) {
    pr_debug("douane(%u):%d:%s: No process_socket_inode with i_ino %lu found\n", packet_id, __LINE__, __FUNCTION__, i_ino);
    return;
  } else {
    pr_debug("douane(%u):%d:%s: Searching old psi found for inode %lu!\n", packet_id, __LINE__, __FUNCTION__, i_ino);
  }

  rcu_read_lock();

  if (old_psi->sequence == sequence) {
    rcu_read_unlock();
    kfree_rcu(old_psi, rcu);
    pr_debug("douane(%u):%d:%s: Skipping update as the sequence %u is already the saved one\n", packet_id, __LINE__, __FUNCTION__, sequence);
    return;
  }

  new_psi = kzalloc(sizeof(struct process_socket_inode), GFP_ATOMIC);
  if (!new_psi) {
    rcu_read_unlock();
    kfree_rcu(old_psi, rcu);
    pr_err("douane(%u):%d:%s: Failed to allocate new process_socket_inode.\n", packet_id, __LINE__, __FUNCTION__);
    return;
  }

  pr_debug("douane(%u):%d:%s: memcpy old_psi into new_psi ...\n", packet_id, __LINE__, __FUNCTION__);
  memcpy(new_psi, old_psi, sizeof(struct process_socket_inode));
  pr_debug("douane(%u):%d:%s: Updating the new_psi sequence from %u to %u ...\n", packet_id, __LINE__, __FUNCTION__, new_psi->sequence, sequence);
  new_psi->sequence = sequence;

  pr_debug("douane(%u):%d:%s: Aquiring lock ...\n", packet_id, __LINE__, __FUNCTION__);
  spin_lock(&psi_lock);
  pr_debug("douane(%u):%d:%s: Replacing old_psi with new_psi ...\n", packet_id, __LINE__, __FUNCTION__);
  list_replace_rcu(&old_psi->list, &new_psi->list);
  pr_debug("douane(%u):%d:%s: Releasing the lock ...\n", packet_id, __LINE__, __FUNCTION__);
  spin_unlock(&psi_lock);

  rcu_read_unlock();

  /*
  ** This module can't block, therefore it can't use synchronize_rcu() and
  *  kfree(), instead it uses kfree_rcu() which creates a callback function to
  *  free the given list entry while in a grace period.
  */
  kfree_rcu(old_psi, rcu);

  pr_debug("douane(%u):%d:%s: We're done!\n", packet_id, __LINE__, __FUNCTION__);
  return;
}
