# Douane DKMS Changelog

## Unreleased

 * Fixes NULL pointer dereference error [#36](https://gitlab.com/douaneapp/douane-dkms/-/issues/36)
 * Removes socket status filering (allowing Douane to filter UDP packes too)
 * Replaces `#ifdef DEBUG printk(...) #endif` by `pr_*()` equivalents [40](https://gitlab.com/douaneapp/douane-dkms/-/issues/40)
 * Reworks the linked list usage with RCU
 * Adds a Network Packet ID in logs in order to trace execution
 * Adds a stucked stack detection script

# v0.8.2 - ?

 * Initial release
